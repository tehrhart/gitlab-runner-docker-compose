# GitLab Runner Docker Compose Setup

This repository provides a Docker Compose setup for deploying a GitLab Runner that utilizes Docker as its executor.

### Prerequisites

- Docker and Docker Compose installed on your host machine.
- A GitLab instance (e.g., GitLab CE, GitLab.com) where you want to register this runner.

### Usage

1. **Clone Repository:**

   ```bash
   git clone https://gitlab.eurecom.fr/tehrhart/gitlab-runner-docker-compose.git
   cd gitlab-runner-docker-compose
   ```

1. **Register Runner:**

   - In your GitLab project or group settings, go to **Settings** -> **CI/CD** -> **Runners**.
   - Click **Register an instance runner**.
   - Choose **Docker** as the executor.
   - Use the tags and description as needed.
   - Copy the provided registration token.

1. **Configure Environment Variables:**

   Copy the `.env.example` file to `.env`:

   ```bash
   cp .env.example .env
   ```

   Edit the `.env` file and set variables as follows:
   - `HOSTNAME`: The desired name for your GitLab Runner (usually the name of the machine hosting it).
   - `RUNNER_URL`: The URL of your GitLab instance (e.g., `https://gitlab.com`).
   - `REGISTRATION_TOKEN`: The registration token obtained from your GitLab project or group settings.- `HOSTNAME`: The desired name for your GitLab Runner (usually the name of the machine hosting it).
      - `RUNNER_URL`: The URL of your GitLab instance (e.g., `https://gitlab.com`).
      - `REGISTRATION_TOKEN`: The registration token obtained from your GitLab project or group settings (see Step 2).

   Optinally, you can also modify the `compose.yml` file to adjust:
   - `concurrent`: The number of jobs the runner can handle simultaneously.
   - Other runner settings in the `config.toml` section, based on the [official GitLab Runner documentation](https://docs.gitlab.com/runner/configuration/advanced-configuration.html).

1. **Start Runner:**

   ```bash
   docker compose up -d
   ```

   This will:
   - Generate the `config.toml` file based on your environment variables.
   - Start the GitLab Runner container.

### How It Works

- **`generate-gitlab-runner-config` Service:** This service creates the `config.toml` file dynamically based on your environment variables. This ensures that the runner configuration is updated every time you start it.

- **`gitlab-runner` Service:** This service runs the actual GitLab Runner container. It mounts the generated configuration file and the Docker socket, allowing it to interact with the Docker daemon to execute CI/CD jobs.

### Troubleshooting

- If you encounter issues, check the logs of the containers:

  ```bash
  docker compose logs
  ```

- If you change the `config.toml` file directly, you'll need to restart the `gitlab-runner` service for the changes to take effect.

